﻿using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace WebApiDemo.Controller
{
    public class ApiAuthorize : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);

            var response = filterContext.Response = filterContext.Response ?? new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.Forbidden;
            var content = new Result
            {
                success = false,
                errs = new[] { "服务端拒绝访问：你没有权限，或者掉线了" }
            };
            response.Content = new StringContent(Json.Encode(content), Encoding.UTF8, "application/json");
        }
    }
}