﻿using System.Collections.Generic;
using System.Web.Http;

namespace WebApiDemo.Controller
{
    public class ValuesController : ApiController
    {
        public IEnumerable<string> Get1()
        {
            return new string[] { "value1", "value2" };
        }

        [ApiAuthorize]
        public IEnumerable<string> Get2()
        {
            return new string[] { "value1", "value2" };
        }

        [Authorize]
        public IEnumerable<string> Get3()
        {
            return new string[] { "value1", "value2" };
        }

    }
}