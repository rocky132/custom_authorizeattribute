﻿using System.Collections.Generic;

namespace WebApiDemo.Controller
{
    public class Result
    {
        public bool success { get; set; }
        public object data { get; set; }
        public string msg { get; set; }
        public IEnumerable<string> errs { get; set; } = new List<string>();
    }
}